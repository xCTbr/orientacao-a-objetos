package br.senai.sp.informatica.oop;

public class CachorroTestDrive {

	public static void main(String[] args) {
		
		// Cria a variavel do objeto especifico
				//Dog
				Cachorro dog = new Cachorro();

				// atribui ra�a ao cachorro
				dog.raca = "Pitbull";
				
				//atribui nome ao bixo
				dog.nome = "Sad�" ;

				// atribui tamanho ao cachorro
				dog.tamanho = 45;

				// chama o metodo
				dog.latir();

				// Wagner
				Cachorro wagner = new Cachorro();
				wagner.raca = "Vira-lata";
				wagner.tamanho = 30;
				wagner.nome = "Wagner";
				wagner.latir();
				
				//Sad�
				Cachorro sada = new Cachorro();
				sada.raca = "Salsicha";
				sada.tamanho = 30;
				sada.nome = "Sad�";
				sada.latir();

	}

}
