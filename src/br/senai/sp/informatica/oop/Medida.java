package br.senai.sp.informatica.oop;

public enum Medida {
	//valores
	MM("Milímetro"), CMI("Centímetro"), M("Metro");
	
	//atributos
	public String titulo;
	
	//construror
	 Medida (String titulo) {
		 this.titulo = titulo;
	 }

}
