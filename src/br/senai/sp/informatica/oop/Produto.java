package br.senai.sp.informatica.oop;
/**
 * 
 * @author Carlos Tofoli
 *
 */
public class Produto {
	
	private String codigo;
	private String descricao;
	private double preco;
	private String categoria;
	private boolean perecivel;
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		if(codigo.length() < 10) {
			System.err.println("C�digo de barras inv�lido");
			this.codigo = codigo;
		}else {
		System.out.println(this.codigo = codigo);
		}
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		if(!descricao.isEmpty() && descricao.length() > 2 && descricao != null) {
		this.descricao = descricao;
		}else {
			System.err.println("Atribua uma descricao ao produto");
		}
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public boolean isPerecivel() {
		return perecivel;
	}
	public void setPerecivel(boolean perecivel) {
		this.perecivel = perecivel;
	}

}
