package br.senai.sp.informatica.oop;

public class Galinha {
	//variavel Global
	public static int ovosDaGranja;
	
	int ovos; // ovos que a galinha botou
	
	public Galinha botar() {
		ovos++;
		ovosDaGranja++;
		return this;
	}
	
	public static double mediaDeOvos (int galinhas) {
		return Galinha.ovosDaGranja / galinhas;
	}
}
