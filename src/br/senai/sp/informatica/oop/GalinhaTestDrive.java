package br.senai.sp.informatica.oop;

import java.util.ArrayList;

public class GalinhaTestDrive {

	public static void main(String[] args) {
		ArrayList<Galinha> lista = new ArrayList<Galinha>();
		Galinha coco = new Galinha();
		coco.botar().botar().botar();
		
		Galinha penosa = new Galinha();
		penosa.botar().botar();
		
		System.out.println("Coc�: " + coco.ovos);
		System.out.println("Penosa: " + penosa.ovos);
		System.out.println("Total de ovos: " + Galinha.ovosDaGranja);
		
		System.out.println("M�dia de ovos: " + Galinha.mediaDeOvos(2));
		
		System.out.println(Galinha.mediaDeOvos(lista.size()));
	}

}
