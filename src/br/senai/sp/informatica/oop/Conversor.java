package br.senai.sp.informatica.oop;

import javax.swing.JOptionPane;

public class Conversor {

	public static void main(String[] args) {
		
		// *Entrada*
				// unidade: unidade de medida
				// temp:temperatura em String
				// temperatura: temperatura convertida de String para Double
				// novamente: Calcular novamente
				int novamente;
				String unidade, temp;
				double celsius, kelvin, fahrenheit, temperatura;

				do {// Inicia o la�o

					unidade = JOptionPane.showInputDialog("Informe a unidade de medida");
					
					if (unidade == null ) {
						break;
					}

					// solicita que o usuario digite a temperatura
					temp = JOptionPane.showInputDialog("Informe a temperatura").replace(",", ".");
					temperatura = Double.parseDouble(temp);

					String msg;

					switch (unidade) {
					case "C":
					case "c":
					case "CELSIUS":
					case "celsius":
						// realiza a conversor para fahrenheit
						fahrenheit = (temperatura * 1.8) + 32;

						// realiza convers�o para kelvin
						kelvin = temperatura + 273.15;

						msg = fahrenheit + " �F\n" + kelvin + " K";
						JOptionPane.showMessageDialog(null, msg);

						break;
					case "F":
					case "f":
					case "FAHREINHEIT":
					case "fahreinheit":
						// converte fahrenheit para celsius
						celsius = (temperatura - 32) / 1.8;

						// converte fahrenheit para kelvin
						kelvin = (temperatura + 459.67) * 0.55555556;

						msg = celsius + " �C\n" + kelvin + " K";
						JOptionPane.showMessageDialog(null, msg);
						break;
					case "K":
					case "k":
					case "KELVIN":
					case "kelvin":
						// converte de kelvin para fahrenheit
						celsius = temperatura - 273.15;

						// converte de kelvin para celsius
						fahrenheit = temperatura * 1.8 - 459.67;

						msg = fahrenheit + " �C\n" + celsius + " �F";
						JOptionPane.showMessageDialog(null, msg);
						break;

					default:
						System.out.println("Unidade inv�lida");
						break;
					}
					novamente = JOptionPane.showConfirmDialog(null, "Deseja calcular novamente?", "Pergunta novamente...",
							JOptionPane.YES_NO_OPTION);
				} while (novamente == 0); // Fecha o laço

	}

}
