package br.senai.sp.informatica.oop;
/**
 * 
 * @author Carlos Tofoli
 * 
 * <ol>
 *		<li></li>
 *		<li></li>
 *		<li></li>
 * </ol>
 *
 */
public class MatematicaTestDrive {

	public static void main(String[] args) {

		// cria uma instancia da classe matematica
		Matematica m = new Matematica();
		int max = m.maior(10, 20);

		System.out.println("Maior: " + max);
		// ou
		System.out.println("Maior: " + m.maior(10, 20));

		// chamando metodo de soma
		double total = m.soma(10, 30);
		System.out.println("Soma: " + total);

		// retorna o maior numero par
		int par = m.maior(2, 4);
		int inpar = m.maior(3, 5);
		double soma = m.soma(par, inpar);
		System.out.println("Soma: " + soma);
		// ou
		System.out.println("Soma: " + m.soma(m.maior(4, 2), m.maior(3, 5)));

		System.out.println("RAIZ de 39 = " + m.raiz(39));

		System.out.println("RAIZ de 39 = " + Math.sqrt(39));
			
		double total2 =m.soma(2,3,4,5);
		System.out.println("Total: " + total2);
		
		double media = m.media(10, 20);
		System.out.println("M�dia: " + media);
		
	}
}
