package br.senai.sp.informatica.oop;
/**
 * 
 * @author Carlos Tofoli
 *
 */
public class Matematica {

	// atributos

	// m�todos
	int maior(int um, int dois) {

		// se o primeiro argumento for maior que o segundo
		if (um > dois) {

			return um;

		} else {

			return dois;

		}

	}

	double soma(double...numeros) {
		double total = 0;
		//percorre o vetor de n�meros somando cada numero encontrado
		for(double numero : numeros) {
			total += numero;
		}
		// ap�s ter somado todos os numeros retorna o total
		return total;

	}

	int raiz(int numero) {

		int raiz = 0;
		int impar = 0;

		while (numero >= impar) {
			numero -= impar; // numero = numero - impar
			impar+= 2;
			++raiz;
		}
		return raiz;

	}
	
	double media(int x, int y) {
		System.out.println("media(int x, int y) ");
		return (x + y) / 2;
	}
	
	double media(String x, String y ) {
		System.out.println("media(String x, String y )");
		int ix = Integer.parseInt(x);
		int iy = Integer.parseInt(y);
		
		return media (ix, iy);
	}

}
