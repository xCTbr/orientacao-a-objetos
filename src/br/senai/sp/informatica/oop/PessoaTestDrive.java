package br.senai.sp.informatica.oop;

import javax.swing.JOptionPane;

public class PessoaTestDrive {

	public static void main(String[] args) {

		// Declara a pessoa Batman
		Pessoa batman = new Pessoa();
		batman.nome = "Batman";
		batman.idade = 30;
		JOptionPane.showMessageDialog(null, batman.nome + " tem " + batman.idade);
		batman.niver();

		// Declara a pessoa Flash
		Pessoa flash = new Pessoa();
		flash.nome = "Flash";
		flash.idade = 23;
		JOptionPane.showMessageDialog(null, flash.nome + " tem " + flash.idade);
		flash.niver();

		// Calcula a diferença de idade entre Batman e Flash
		int diferenca = (batman.idade - flash.idade);
		JOptionPane.showMessageDialog(null,
				"A diferen�a entre " + batman.nome + " e " + flash.nome + " � de " + diferenca + " anos");

	}

}
