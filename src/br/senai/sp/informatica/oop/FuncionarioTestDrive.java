package br.senai.sp.informatica.oop;

public class FuncionarioTestDrive {

	public static void main(String[] args) {
		
		Funcionario f = new Funcionario();
		f.setNome("Jo�o");
		f.setAtivo(true);
		f.setSalario(2_650.00);
		f.setSenha("123");
		
		System.out.println("Nome: " + f.getNome());
		System.out.println("Ativo: " + f.isAtivo());
		System.out.println("Salario: " + f.getSalario());
		System.out.println("Senha: " + f.getSenha());

	}

}
