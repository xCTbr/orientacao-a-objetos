package br.senai.sp.informatica.oop;

public class EnumTest {
	
	public enum Sexo {
		MASCULINO("Masculino"), FEMININO("Feminino");
		
		public String s;
		
		Sexo(String s) {
			this.s = s;
		}
	}
	
	//constante
	public static final double PI = 3.14;
	
	public static void main(String[] args) {
		System.out.println(PecasXadrez.RAINHA);
		System.out.println(PecasXadrez.REI);
		
		System.out.println(Medida.CMI.titulo);
		
		for (Medida m : Medida.values()) {
			System.out.println(m + " : " + m.titulo);
		}
	}
	
	//percorre a Enum mostrando os valores
}
