package br.senai.sp.informatica.oop;

public class Funcionario {

	private String nome;
	private double salario;
	private boolean ativo;
	private String senha;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		if (!nome.isEmpty() && nome.length() > 2 && nome != null) {
			this.nome = nome;
		} else {
			this.nome = "";
		}
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		if (salario >= 937) {
			this.salario = salario;
		} else {
			System.out.println("Valor Inv�lido ");
		}
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	// Getters & Setters

}